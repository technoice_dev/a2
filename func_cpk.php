<?php
function cpk_inv($new_ttl,$mileage,$unit,$contract) {
include "connectdatabase.php";

$pembagi = $mileage * 12 * $contract * $unit;
$cpk = $new_ttl / $pembagi;
?>
<!--<div class="row">-->
<!---------- CONTENT MASTER UNIT --------------->
	<div class="col-lg-6">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-car"></i> Investment Costs / Unit</h3>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-bordered table-hover tablesorter">
						<thead>
							<tr class="bg-primary"><th>Item</th><th>Total Costs</th></tr>
						</thead>
						<tbody>
							<tr><td>Tools + Equipment</td><td><?php echo number_format($new_ttl); ?></td></tr>
						</tbody>
						<tfoot>
							<tr class="bg-warning"><td>CPK</td><td><?php echo number_format($cpk); ?></td></tr>
						</tfoot>
					</table>
				</div>
				<div class="text-right">
			
				</div>				
			</div>
		</div>
	</div>
<!--</div><!-- /.row -->
<?php 
Return $cpk;
}
?>
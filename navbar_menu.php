<!doctype html>
<html>
<style script="text/css">
.vertikal ul {
	list-style-type: none;
	border: 1px solid black;
	width: 200px;
	margin: 0;
	padding: 0;
	font-family: arial;
	font-size: 16px;
	font-weight: bold;
	background-color:  #a09c9c ;
}

.vertikal ul li a {
	display: block;
	color: black;
	margin: 5px 7px;
	text-decoration: none;

	padding: 5px;
}

.vertikal ul li a:hover {
	background-color: orange;
	color: white;
}
</style>
<nav class="vertikal" style="padding: 0px 0px">
<ul>
<li><a href="#">Islam</a></li>
<li><a href="#">Ekonomi</a></li>
<li><a href="#">Kesehatan</a></li>
<li><a href="#">Motivasi</a></li>
<li><a href="#">Travelling</a></li>
</ul>
</nav>
</html>
<?php
include "connectdatabase.php";
include "func_mtc_table.php";
include "func_mtc_notable.php";
include "func_rpr_table.php";
include "func_rpr_notable.php";
include "func_mr_cpk.php";
include "func_manpower_notable.php";
include "func_manpower_table.php";
include "func_inv_table.php";
include "func_inv_notable.php";
include "func_inv_cpk.php";

$pTipe = $_POST['tipe'];
$pDisc = $_POST['disc'];
$R = 100 - $pDisc;
$pJumlah = $_POST['jumlah'];
$pTempuh = ($_POST['jarak']) * ($_POST['day']);
$pKontrak = $_POST['kontrak'];
$pSenior = $_POST['senior'];
$pJunior = $_POST['junior'];
//echo "Rate : ".$R;
?>

<link href = "bootstrap/3.3.7/css/bootstrap.min.css" rel = "stylesheet">
<script src = "bootstrap/3.3.7/js/jquery-3.3.1.min.js"></script>
<script src = "bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- 1. MAINTENANCE -->
<h4>
<div class="row"><div class="col-lg-2"> JUMLAH UNIT</div><div>: <?php echo $pJumlah; ?> Unit</div></div>
<div class="row"><div class="col-lg-2"> DISCOUNT</div><div>: <?php echo $pDisc; ?> %</div></div>
<div class="row"><div class="col-lg-2"> MILEAGE</div><div>: <?php echo $pTempuh; ?> Km/bulan</div></div>
<div class="row"><div class="col-lg-2"> KONTRAK</div><div>: <?php echo $pKontrak; ?> Tahun</div></div>
</h4>
<p>
<!-- prosedur mtc_table -->
<!--- ROW 1 ----->
<div class="row">
	<div class="col-lg-12">
		<?php
			$mt = mtc_notable($pTipe,$pDisc,$pKontrak);
			$rt = repair_notable($pTipe,$pDisc,$pKontrak);
			$cpkpart = cpk_part($pTipe,$mt,$rt,$pTempuh,$pJumlah,$pKontrak);
			//manpower_table($pSenior,$pJunior,$pJumlah,$pTempuh,$pKontrak);
			manpower_notable($pSenior,$pJunior,$pJumlah,$pTempuh,$pKontrak);
			//inv_table();
			$ttl_cost = inv_notable();
			$cpkinv = cpk_inv($ttl_cost,$pJumlah,$pTempuh,$pKontrak);
		?>
	</div>
</div>
<!--- ROW 2 ----->
<div class="row">
	<div class="col-lg-4">
	<a href="#">MaintenancePlanner</a>
	</div>
	<div class="col-lg-4">

	</div>
	<div class="col-lg-4">

	</div>	
</div>
<!--- ROW 3 ----->
<div class="row">
	<div class="col-lg-12">
	
	</div>
</div>
</p>
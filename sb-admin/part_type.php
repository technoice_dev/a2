<?php
include "connectdatabase.php";
?>
<!doctype html>
<html>

<head>
	<meta charset="utf-8">    
	<meta http-equiv="X-UA-Compatible" content="IE=edge">    
	<meta name="viewport" content="width=device-width, initial-scale=1">  
	<link href = "bootstrap/3.3.7/css/bootstrap.min.css" rel = "stylesheet">
	<script src = "bootstrap/3.3.7/js/jquery-3.3.1.min.js"></script>
	<script src = "bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" async="" src="js/search.js"></script>
</head>
<style type="text/css">
<!--
table {
    width: 100%;
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	table-layout: fixed;
}
th, td {
    text-align: left;
    padding: 4px 4px;
//	border: 1px solid #ddd;
}
th {
    background-color: #2e86c1;
    color: white;
}
td {
    background-color: #ffffff;
    color: black;
}
<!-- SCROLL TABLE -->
table.scroll {width: 100%; border: 1px #a9c6c9 solid; font-family: "Trebuchet MS",arial,sans-serif;}
table.scroll thead {display: table; width: 100%;}
table.scroll tbody {display: block; height: 500px; overflow: auto; float: left;width:100%;}
table.scroll tbody tr {display: table; width: 100%;}
table.scroll th, td {width: 10%; padding: 10px 4px;}
table.scroll th {background-color: #2e86c1; color: white;}
table.scroll tr:hover td {background:#a0a0a0;color:#ffffff;}
table.scroll tr:nth-child(odd) {background-color:#c0c0c0;}
table.scroll tr:nth-child(even) {background-color:#f0f0f0;}
</style>
<script>
	function hanyaAngka(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	}
</script>
<body>
<h2><center><font face="Britannic Bold">INPUT PART PER TYPE</font></center></h2>
<form method="post" action = "">
<p style="padding: 4px">Pilih Type :
<select name="jenis" style="padding: 4px 4px" onchange = "this.form.submit()" value="" autofocus required>
<option value="">Pilih Jenis / Type ...</option>
<?php
$type = mysqli_query($conn,"select * from unitmaster");
while($cType = mysqli_fetch_array($type)) {
	if ($cType[1] == $_POST['jenis']) 
		echo "<option value='$cType[1]' selected>$cType[2]</option>";
	else
		echo "<option value='$cType[1]'>$cType[2]</option>";
}
?>
</select></p>
<table class="scroll">
	<thead>
		<tr>
			<th style="width: 2%">NO</th><th style="width: 20%">PART NAME</th><th>PART NO.</th><th>QTY</th><th>INTERVAL CHANGE (KM)</th><th>PROBABILITIES</th>
		</tr>
	</thead>
	<tbody>	
	<?php
	$no = 1;
	if (empty($_POST['jenis'])) {
		$type = mysqli_query($conn,"select * from parttype where unit_type = ''");
	} else {
		$type = mysqli_query($conn,"select * from parttype where unit_type = '$_POST[jenis]'");
	}
	if (mysqli_num_rows($type) == 0) {
		$part = mysqli_query($conn,"select * from partmaster");
		while ($cPart = mysqli_fetch_array($part)) {
			echo "<tr><td style='width: 2%'>$no</td><td style='width: 20%'>$cPart[part_name]</td><td>$cPart[part_no]</td>";
			echo "<td><input type='text' class='form-control' name='qty' value=0 onkeypress='return hanyaAngka(event)' required></td>";
			echo "<td><input type='text' class='form-control' name='interval' value=0 onkeypress='return hanyaAngka(event)' required></td>";
			echo "<td><input type='text' class='form-control' name='prob' value=0 onkeypress='return hanyaAngka(event)' required></td>";
			$no++;
		}
	} else {	
		while ($cType = mysqli_fetch_array($type)) {
			$partname = mysqli_query($conn,"select * from partmaster where part_no = '$cType[part_no]'");
			$cName = mysqli_fetch_array($partname); $cInterval = number_format($cType['kminterval']);
			echo "<tr><td style='width: 2%'>$no</td><td style='width: 20%'>$cName[part_name]</td><td>$cType[part_no]</td>";
			echo "<td><input type='text' class='form-control' name='qty' value='$cType[qty]' onkeypress='return hanyaAngka(event)' required></td>";
			echo "<td><input type='text' class='form-control' name='interval' value='$cInterval' onkeypress='return hanyaAngka(event)' required></td>";
			echo "<td><input type='text' class='form-control' name='prob' value='$cType[probabilitas]'  onkeypress='return hanyaAngka(event)' requires></td>";
			$no++;
		}		
	}
	?>
	</tbody>
</table>

<hr>
</div>
<p align="right"><button type="submit" class="btn btn-danger">SAVE</button>&nbsp;<button type="button" class="btn btn-default">HOME</button>&nbsp;</p>
</form>
</body>
</html>


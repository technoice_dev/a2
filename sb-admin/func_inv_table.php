<?php
function inv_table() {
include "connectdatabase.php";

echo "<table class='table table-striped table-hover'>
<thead>
<tr>
<th>No</th><th>Item</th><th>Price</th><th>Qty</th><th>Sub Total</th></tr>
</tr>
</thead>
<tbody>";
$no = 1; 
/******************** GENERAL **************************************/
$tsubgl = 0;
echo "<tr><th colspan=5>I. General Tools</th></tr>";
$general = mysqli_query($conn,"select * from inv_general");
while ($gl = mysqli_fetch_array($general)) {
	$subgl = $gl['inv_price'] * $gl['inv_qty'];
	$vPricegl = number_format($gl['inv_price']);$vSubgl = number_format($subgl);
	echo "<tr>";
	echo "<td>$no</td><td>$gl[inv_item]</td><td style='text-align: right'>$vPricegl</td><td style='text-align: center'>$gl[inv_qty]</td><td style='text-align: right'>$vSubgl</td>";
	echo "</tr>";
	echo "<tr>";
	echo "<tr>";
	$no++; $tsubgl = $tsubgl + $subgl; 
}
$vTotalgl = number_format($tsubgl);
echo "<tr><td colspan=4 style='background-color: #FF5733; color: white; text-align: center'><b>TOTAL</b></td>";
echo "<td style='text-align: right'>$vTotalgl</td></tr>";
echo "<tr><td colspan=5></td></tr>";
/******************* POWER **************************************/
$tsubpwr = 0;
echo "<tr><th colspan=5>II. Power Tools</th></tr>";
$power = mysqli_query($conn,"select * from inv_power");
while ($pwr = mysqli_fetch_array($power)) {
	$subpwr = $pwr['inv_price'] * $pwr['inv_qty'];
	$vPricepwr = number_format($pwr['inv_price']);$vSubpwr = number_format($subpwr);
	echo "<tr>";
	echo "<td>$no</td><td>$pwr[inv_item]</td><td style='text-align: right'>$vPricepwr</td><td style='text-align: center'>$pwr[inv_qty]</td><td style='text-align: right'>$vSubpwr</td>";
	echo "</tr>";
	echo "<tr>";
	echo "<tr>";
	$no++; $tsubpwr = $tsubpwr + $subpwr; 
}
$vTotalpwr = number_format($tsubpwr);
echo "<tr><td colspan=4 style='background-color: #FF5733; color: white; text-align: center'><b>TOTAL</b></td>";
echo "<td style='text-align: right'>$vTotalpwr</td></tr>";
/******************* SAFETY **************************************/
$tsubsy = 0;
echo "<tr><th colspan=5>III. Safety Facilities</th></tr>";
$safety = mysqli_query($conn,"select * from inv_safety");
while ($sy = mysqli_fetch_array($safety)) {
	$subsy = $sy['inv_price'] * $sy['inv_qty'];
	$vPricesy = number_format($sy['inv_price']);$vSubsy = number_format($subsy);
	echo "<tr>";
	echo "<td>$no</td><td>$sy[inv_item]</td><td style='text-align: right'>$vPricesy</td><td style='text-align: center'>$sy[inv_qty]</td><td style='text-align: right'>$vSubsy</td>";
	echo "</tr>";
	echo "<tr>";
	echo "<tr>";
	$no++; $tsubsy = $tsubsy + $subsy; 
}
$vTotalsy = number_format($tsubsy);
echo "<tr><td colspan=4 style='background-color: #FF5733; color: white; text-align: center'><b>TOTAL</b></td>";
echo "<td style='text-align: right'>$vTotalsy</td></tr>";
/******************* MEASURING **************************************/
$tsubms = 0;
echo "<tr><th colspan=5>IV. Measuring Tools</th></tr>";
$measuring = mysqli_query($conn,"select * from inv_measuring");
while ($ms = mysqli_fetch_array($measuring)) {
	$subms = $ms['inv_price'] * $ms['inv_qty'];
	$vPricems = number_format($ms['inv_price']);$vSubms = number_format($subms);
	echo "<tr>";
	echo "<td>$no</td><td>$ms[inv_item]</td><td style='text-align: right'>$vPricems</td><td style='text-align: center'>$ms[inv_qty]</td><td style='text-align: right'>$vSubms</td>";
	echo "</tr>";
	echo "<tr>";
	echo "<tr>";
	$no++; $tsubms = $tsubms + $subms; 
}
$vTotalms = number_format($tsubms);
echo "<tr><td colspan=4 style='background-color: #FF5733; color: white; text-align: center'><b>TOTAL</b></td>";
echo "<td style='text-align: right'>$vTotalms</td></tr>";
/******************* SST DUTRO **************************************/
$tsubdt = 0;
echo "<tr><th colspan=5>IV.1. SST Dutro</th></tr>";
$dutro = mysqli_query($conn,"select * from inv_sst_dutro");
while ($dt = mysqli_fetch_array($dutro)) {
	$subdt = $dt['inv_price'] * $dt['inv_qty'];
	$vPricedt = number_format($dt['inv_price']);$vSubdt = number_format($subdt);
	echo "<tr>";
	echo "<td>$no</td><td>$dt[inv_item]</td><td style='text-align: right'>$vPricedt</td><td style='text-align: center'>$dt[inv_qty]</td><td style='text-align: right'>$vSubdt</td>";
	echo "</tr>";
	echo "<tr>";
	echo "<tr>";
	$no++; $tsubdt = $tsubdt + $subdt; 
}
$vTotaldt = number_format($tsubdt);
echo "<tr><td colspan=4 style='background-color: #FF5733; color: white; text-align: center'><b>TOTAL</b></td>";
echo "<td style='text-align: right'>$vTotaldt</td></tr>";
/******************* SST RANGER **************************************/
$tsubrg = 0;
echo "<tr><th colspan=5>IV.2. SST Ranger</th></tr>";
$ranger = mysqli_query($conn,"select * from inv_sst_ranger");
while ($rg = mysqli_fetch_array($ranger)) {
	$subrg = $rg['inv_price'] * $rg['inv_qty'];
	$vPricerg = number_format($rg['inv_price']);$vSubrg = number_format($subrg);
	echo "<tr>";
	echo "<td>$no</td><td>$rg[inv_item]</td><td style='text-align: right'>$vPricerg</td><td style='text-align: center'>$rg[inv_qty]</td><td style='text-align: right'>$vSubrg</td>";
	echo "</tr>";
	echo "<tr>";
	echo "<tr>";
	$no++; $tsubrg = $tsubrg + $subrg; 
}
$vTotalrg = number_format($tsubrg);
echo "<tr><td colspan=4 style='background-color: #FF5733; color: white; text-align: center'><b>TOTAL</b></td>";
echo "<td style='text-align: right'>$vTotalrg</td></tr>";
/******************* OTHERS **************************************/
$tsubot = 0;
echo "<tr><th colspan=5>V. Others</th></tr>";
$others = mysqli_query($conn,"select * from inv_others");
while ($ot = mysqli_fetch_array($others)) {
	$subot = $ot['inv_price'] * $ot['inv_qty'];
	$vPriceot = number_format($ot['inv_price']);$vSubot = number_format($subot);
	echo "<tr>";
	echo "<td>$no</td><td>$ot[inv_item]</td><td style='text-align: right'>$vPriceot</td><td style='text-align: center'>$ot[inv_qty]</td><td style='text-align: right'>$vSubot</td>";
	echo "</tr>";
	echo "<tr>";
	echo "<tr>";
	$no++; $tsubot = $tsubot + $subot; 
}
$vTotalot = number_format($tsubot);
echo "<tr><td colspan=4 style='background-color: #FF5733; color: white; text-align: center'><b>TOTAL</b></td>";
echo "<td style='text-align: right'>$vTotalot</td></tr>";
$TotalAll = $tsubgl + $tsubpwr + $tsubsy + $tsubms + $tsubdt + $tsubrg + $tsubot;
$TTLall = number_format($TotalAll);
echo "<tr><td colspan=4 style='background-color: #FBA500; color: white; text-align: center'><b>TOTAL ALL</b></td>";
echo "<td style='text-align: right; border: solid 1px #000'><b>$TTLall</b></td></tr>";
echo "
</tbody>
</table>";
}
?>
<?php
function mtc_table($new_tipe,$new_disc,$term) {
include "connectdatabase.php";

$no = 1; $limit = $term + 1;
ECHO "<p><h2>MAINTENANCE</H2></P>";
echo "<table class='table table-striped'>
<thead>
<tr>
<th>No</th><th>Part Name</th><th>Qty</th><th>Price</th><th>Total Price After Disc.</th><th>Changeable Freq. (/Yr)</th><th>Prob</th>";
for ($y = 1; $y < $limit; $y++) {
	//$i[$y] = 0;
	echo "<th>Tahun ke ".$y."</th>";
}
echo "
</tr>
</thead>
<tbody>";
for ($y = 1; $y < $limit; $y++) {
	$i[$y] = 0;
	$J[$y] = $i[$y];
}
$part = mysqli_query($conn,"select * from part_mtc where unit_type = '$new_tipe'");
while ($row = mysqli_fetch_array($part)) {
	$pmaster = mysqli_query($conn,"select * from partmaster_mtc where part_no = '$row[part_no]'");
	$brs = mysqli_fetch_array($pmaster);
	$harga = number_format($brs['pricelist']);
	if($row['discincl'] == 1)
		$newprice = $brs['pricelist'] * ((100-$new_disc)/100) * $row['qty'];
	else
		$newprice = $brs['pricelist'] * $row['qty'];
	$discprice = number_format($newprice);
	$freq1 = $brs['km1th'] / $row['kminterval']; $freq = number_format($freq1,2);
	$Y1 = $newprice * $freq1 * $row['probabilitas'];
	echo "<tr>";
	echo "<td>$no</td><td>$brs[part_name]</td><td>$row[qty]</td><td style='text-align: right'>$harga</td><td style='text-align: right'>$discprice</td><td style='text-align: center'>$freq</td><td style='text-align: center'>$row[probabilitas]</td>";
	if ($term > 1) {
		$x = 1; $Y[$x] = $Y1; 
		//echo $Y[$x]."<br>";
		for ($z = 1; $z < $limit; $z++) {
			//$Y[$z-1] = $Y1;
			$Y[$z+1] = $Y[$z] * 108/100;
			$Yr[$z] = number_format($Y[$z]);			
			echo "<td style='text-align: center'>".$Yr[$z]."</td>";
			$J[$x] += $Y[$x]; $x++;
		}
	}
	echo "</tr>";
	$no++; 
}
$Total = 0;
echo "<tr><td colspan=7><b>TOTAL PER YEAR</b></td>";
for ($v = 1; $v < $limit; $v++) {
		//$A[$v] = array($Y[$v]);$J[$v] = array_sum($A[$v]);
		$xJ[$v] = number_format($J[$v]);
		echo "<td style='text-align: center'><b>".$xJ[$v]."</b></td>";
		$Total = $Total + $J[$v];	
}
echo "</tr>";
$TTL = number_format($Total);
echo "<tr><td colspan=7 style='background-color: #FFC300; color: white; text-align: center;border:2px groove #000;'><b>TOTAL</b></td><td style='text-align: center;border:2px groove #000;' colspan=5>$TTL</td></tr>";
echo "
</tbody>
</table>";
return $Total;
}
?>
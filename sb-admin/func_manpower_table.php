<?php
function manpower_table($new_senior,$new_junior,$new_uio,$new_mileage,$term) {
include "connectdatabase.php";

$no = 1; $limit = $term + 1; $srSub = 0; $jrSub = 0; $allSub = 0;
echo "<table class='table table-striped'>
<thead>
<tr>
<th rowspan=2>No</th><th rowspan=2>Item</th><th>Senior Mechanics</th><th>Junior Mechanics</th><th>All Mechanics</th>";

for ($y = 1; $y < $limit; $y++) {
	//$i[$y] = 0;
	echo "<th rowspan=2>Tahun ke ".$y."</th>";
}
echo "
</tr>
<tr>
<th>Rp / month</th><th>Rp / month</th><th>Rp / month</th>
</tr>
</thead>
<tbody>";
/********** identify salary ************/
$salary = mysqli_query($conn,"select * from manpower where item = 'Bsc Salary'");
$sal = mysqli_fetch_array($salary);
$srSalary = $sal['mek_senior'];$jrSalary = $sal['mek_junior'];
/********** identify meal allowance  ************/
$meal = mysqli_query($conn,"select * from manpower where item = 'Daily Meal'");
$daily = mysqli_fetch_array($meal);
$srMeal = $daily['mek_senior'];$jrMeal = $daily['mek_junior'];

/************************************************/
for ($y = 1; $y < $limit; $y++) {
	$i[$y] = 0;
	$J[$y] = $i[$y];
}
$mp = mysqli_query($conn,"select * from manpower where item != 'Daily Meal'");
while ($row = mysqli_fetch_array($mp)) {
/*
PPH 	(SALARY + THR) X 5%
MEAL ALLOWANCE 	DAILY MEAL X 30 DAYS
MEDICAL ALLOWANCE 	(SALARY X 2) / 12 MONTH
BPJS KESEHATAN + T. KERJA 	SALARY X 11%
THR 	SALARY / 12 MONTH	
*/	
	if($row['item'] == 'THR') {
		$row['mek_senior'] = $srSalary / 12;
		$row['mek_junior'] = $jrSalary / 12;
	}
	if($row['item'] == 'PPH') {
		$row['mek_senior'] = (5/100) * ($srSalary + ($srSalary / 12));
		$row['mek_junior'] = (5/100) * ($jrSalary + ($jrSalary / 12));
	}
	if($row['item'] == 'Meal Allowance') {
		$row['mek_senior'] = $srMeal * 30;
		$row['mek_junior'] = $jrMeal * 30;
	}
	if($row['item'] == 'Medical Allowance') {
		$row['mek_senior'] = ($srSalary * 2) / 12;
		$row['mek_junior'] = ($jrSalary * 2) / 12;
	}
	if($row['item'] == 'BPJS Kesehatan and T. Kerja') {
		$row['mek_senior'] = $srSalary * 11/100;
		$row['mek_junior'] = $jrSalary * 11/100;
	}	
	$srPay = number_format($row['mek_senior']);$jrPay = number_format($row['mek_junior']);
	$all = ($row['mek_senior'] * $new_senior) + ($row['mek_junior'] * $new_junior);$allPay = number_format($all);
	$Y1 = $all * 12; //$Y2 = $Y1 * 1; $Y3 = $Y2 * 1; $Y4 = $Y3 * 1; $Y5 = $Y4 * 1;
	echo "<tr>
	<td>$no</td><td style='text-align: left'>$row[item]</td><td style='text-align: right'>$srPay</td><td style='text-align: right'>$jrPay</td><td style='text-align: right'>$allPay</td>";
	if ($term > 1) {
		$x = 1; $Y[$x] = $Y1; 
		//echo $Y[$x]."<br>";
		for ($z = 1; $z < $limit; $z++) {
			//$Y[$z-1] = $Y1;
			$Y[$z+1] = $Y[$z];
			$Yr[$z] = number_format($Y[$z]);			
			echo "<td style='text-align: right'>".$Yr[$z]."</td>";
			$J[$x] += $Y[$x]; $x++;
			
		}
	}
	echo "</tr>";
	$no++; 
	$srSub = $row['mek_senior'] + $srSub; $jrSub = $row['mek_junior'] + $jrSub; $allSub = $all + $allSub;
	$sS = number_format($srSub); $jS = number_format($jrSub); $aS = number_format($allSub); 
}
$Total = 0;$srSubM = $srSub * 1.2; $jrSubM = $jrSub * 1.2; $allSubM = $allSub * 1.2; $X1 = $allSubM * 12;
echo "<tr><td colspan=2 style='background-color: #FFC300; color: white; text-align: center'><b>SUB TOTAL</b></td>";
echo "<td style='text-align: right'>$sS</td><td style='text-align: right'>$jS</td><td style='text-align: right'>$aS</td>";
for ($v = 1; $v < $limit; $v++) {
		$xJ[$v] = number_format($J[$v]);
		echo "<td style='text-align: right'><b>".$xJ[$v]."</b></td>";
		$X[$v] = $allSubM * 12;
		$Total = $Total + $X[$v];	
}
$TM = number_format($Total);
$sSM = number_format($srSubM); $jSM = number_format($jrSubM); $aSM = number_format($allSubM); 
echo "<tr><td colspan=2 style='background-color: #FF5733; color: white; text-align: center'><b>TOTAL CHARGES</b></td>";
echo "<td style='text-align: right'>$sSM</td><td style='text-align: right'>$jSM</td><td style='text-align: right'>$aSM</td>";
for ($b=1; $b < $limit; $b++) {
	$Xx[$b] = number_format($X[$b]);
		echo "<td style='text-align: right'><b>".$Xx[$b]."</b></td>";
}
echo "<tr><td colspan=5 style='background-color: #FBA500; color: white; text-align: center;border:2px groove #000;'><b>TOTAL</b></td><td style='text-align: center;border:2px groove #000;' colspan=5><b>$TM</b></td></tr>";
echo "
</tbody>
</table>";
}
?>
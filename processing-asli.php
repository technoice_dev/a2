<?php
include "connectdatabase.php";
$pTipe = $_POST['tipe'];
$pDisc = $_POST['disc'];
$R = 100 - $pDisc;
//echo "Rate : ".$R;
?>

<link href = "bootstrap/3.3.7/css/bootstrap.min.css" rel = "stylesheet">
<script src = "bootstrap/3.3.7/js/jquery-3.3.1.min.js"></script>
<script src = "bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
table {
    width: 100%;
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
}
th, td {
    padding: 4px 4px;
}
th {
    background-color: #2e86c1;
    color: white;
	text-align: center;
}
td {
    background-color: #ffffff;
    color: black;
}
</style>
<!-- 1. MAINTENANCE -->
<h4>JENIS : <?php echo $pTipe; ?></h4>
DISCOUNT : <?php echo $pDisc; ?>%
<br>
<table class="table table-striped">
<thead>
<tr>
<th>No</th><th>Part Name</th><th>Qty</th><th>Price</th><th>Price After Disc.</th><th>Changeable Freq. (/Yr)</th><th>Year 1st</th><th>Year 2nd</th><th>Year 3rd</th><th>Year 4th</th><th>Year 5th</th>
</tr>
</thead>
<tbody>
<?php
$no = 1;$a = 0; $b = 0; $c = 0; $d = 0; $e = 0;
$part = mysqli_query($conn,"select * from parttype where unit_type = '$pTipe'");
while ($row = mysqli_fetch_array($part)) {
	$pmaster = mysqli_query($conn,"select * from partmaster where part_no = '$row[part_no]'");
	$brs = mysqli_fetch_array($pmaster);
	$harga = number_format($brs['pricelist']);
	if($row['discincl'] == 1)
		$newprice = $brs['pricelist'] * ((100-$pDisc)/100) * $row['qty'];
	else
		$newprice = $brs['pricelist'] * $row['qty'];
	$discprice = number_format($newprice);
	$freq = $brs['km1th'] / $row['kminterval'];
	$Y1 = $newprice * $freq * $row['probabilitas'];
	$Y2 = $Y1 * (108/100); $Y3 = $Y2 * (108/100);$Y4 = $Y3 * (108/100);$Y5 = $Y4 * (108/100);
	$Yr1 = number_format($Y1);$Yr2 = number_format($Y2);$Yr3 = number_format($Y3);$Yr4 = number_format($Y4);$Yr5 = number_format($Y5);
	echo "<tr>";
	echo "<td>$no</td><td>$brs[part_name]</td><td>$row[qty]</td><td style='text-align: right'>$harga</td><td style='text-align: right'>$discprice</td><td style='text-align: center'>$freq</td><td style='text-align: right'>$Yr1</td><td style='text-align: right'>$Yr2</td><td style='text-align: right'>$Yr3</td><td style='text-align: right'>$Yr4</td><td style='text-align: right'>$Yr5</td>";
	echo "</tr>";
	echo "<tr>";
	echo "<tr>";
	$no++; $a = $Y1 + $a; $b = $Y2 + $b; $c = $Y3 + $c; $d = $Y4 + $d; $e = $Y5 + $e; $TTL = $a + $b + $c + $d + $e;
	$aa = number_format($a);$bb = number_format($b);$cc = number_format($c);$dd = number_format($d);$ee = number_format($e); $T = number_format($TTL);
}
echo "<tr><td colspan=6 style='background-color: #FF5733; color: white; text-align: center'><b>TOTAL PER YEAR</b></td>";
echo "<td style='text-align: right'>$aa</td><td style='text-align: right'>$bb</td><td style='text-align: right'>$cc</td><td style='text-align: right'>$dd</td><td style='text-align: right'>$ee</td></tr>";
echo "<tr><td colspan=6 style='background-color: #FFC300; color: white; text-align: center;border:2px groove #000;'><b>TOTAL</b></td><td style='text-align: center;border:2px groove #000;' colspan=5>$T</td>";

?>
</tbody>
</table>
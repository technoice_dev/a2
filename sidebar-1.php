<!doctype HTML>
<html>

<head>    
	<meta charset="utf-8">    
	<meta http-equiv="X-UA-Compatible" content="IE=edge">    
	<meta name="viewport" content="width=device-width, initial-scale=1">  
	<link href = "bootstrap/3.3.7/css/bootstrap.min.css" rel = "stylesheet">
	<script src = "bootstrap/3.3.7/js/jquery-3.3.1.min.js"></script>
	<script src = "bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link href = "css/sidebar-1.css" rel = "stylesheet">
	<!--<script src = "js/sidebar-wrapper.js"></script>-->
</head>

<body>
<div class="container">
	<div class="row">
		<div class="left ">
        <ul>
            <a href="#">
                <li class="item-menu">
                    <span class="glyphicon glyphicon-home"></span> 
                    <span class="menu">Trang Chủ</span>
                </li>
            </a> 
            <a href="#">
                <li class="item-menu">
                    <span class="glyphicon glyphicon-book"></span> 
                    <span class="menu">Manga</span>
                </li>
            </a>
            <a href="#">
                <li class="item-menu">
                    <span class="glyphicon glyphicon-pencil"></span> 
                    <span class="menu">Light Novel</span>
                </li>
            </a>    
            <a href="#">
                <li class="item-menu">
                    <span class="glyphicon glyphicon-list"></span> 
                    <span class="menu">Danh Sách</span>
                </li>
            </a> 
            <a href="#">
                <li class="item-menu">
                    <span class="glyphicon glyphicon-envelope"></span> 
                    <span class="menu"> Liên Hệ</span>
                </li>
            </a>
            <a href="#">
                <li class="item-menu">  
                    <span class="glyphicon glyphicon-credit-card"></span> 
                    <span class="menu">Ủng Hộ</span>
                </li>
            </a> 
            <li class="item-menu"> 
                 <span class="glyphicon glyphicon-search"></span> 
                 <input class="t_search" type="text" placeholder="Tìm Kiếm">
            </li>
        </ul>
    </div> <!-- end left -->
    <div class="right">
        content
    </div>
    </div>
	</div>
</div>

</body>
</html>
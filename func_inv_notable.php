<?php
function inv_notable() {
include "connectdatabase.php";

/******************** GENERAL **************************************/
$tsubgl = 0;
$general = mysqli_query($conn,"select * from inv_general");
while ($gl = mysqli_fetch_array($general)) {
	$subgl = $gl['inv_price'] * $gl['inv_qty'];
	$tsubgl = $tsubgl + $subgl; 
}
/******************* POWER **************************************/
$tsubpwr = 0;
$power = mysqli_query($conn,"select * from inv_power");
while ($pwr = mysqli_fetch_array($power)) {
	$subpwr = $pwr['inv_price'] * $pwr['inv_qty'];
	$tsubpwr = $tsubpwr + $subpwr; 
}
/******************* SAFETY **************************************/
$tsubsy = 0;
$safety = mysqli_query($conn,"select * from inv_safety");
while ($sy = mysqli_fetch_array($safety)) {
	$subsy = $sy['inv_price'] * $sy['inv_qty'];
	$tsubsy = $tsubsy + $subsy; 
}
/******************* MEASURING **************************************/
$tsubms = 0;
$measuring = mysqli_query($conn,"select * from inv_measuring");
while ($ms = mysqli_fetch_array($measuring)) {
	$subms = $ms['inv_price'] * $ms['inv_qty'];
	$tsubms = $tsubms + $subms; 
}
/******************* SST DUTRO **************************************/
$tsubdt = 0;
$dutro = mysqli_query($conn,"select * from inv_sst_dutro");
while ($dt = mysqli_fetch_array($dutro)) {
	$subdt = $dt['inv_price'] * $dt['inv_qty'];
	$tsubdt = $tsubdt + $subdt; 
}
/******************* SST RANGER **************************************/
$tsubrg = 0;
$ranger = mysqli_query($conn,"select * from inv_sst_ranger");
while ($rg = mysqli_fetch_array($ranger)) {
	$subrg = $rg['inv_price'] * $rg['inv_qty'];
	$tsubrg = $tsubrg + $subrg; 
}
/******************* OTHERS **************************************/
$tsubot = 0;
$others = mysqli_query($conn,"select * from inv_others");
while ($ot = mysqli_fetch_array($others)) {
	$subot = $ot['inv_price'] * $ot['inv_qty'];
	$tsubot = $tsubot + $subot; 
}
/*****************************************************************/
$TTL = $tsubgl + $tsubpwr + $tsubsy +$tsubms + $tsubdt + $tsubrg + $tsubot;
return $TTL;
}
?>
<?php
function manpower_notable($new_senior,$new_junior,$new_uio,$new_mileage,$term) {
include "connectdatabase.php";

$no = 1; $limit = $term + 1; $srSub = 0; $jrSub = 0; $allSub = 0;
/********** identify salary ************/
$salary = mysqli_query($conn,"select * from manpower where item = 'Bsc Salary'");
$sal = mysqli_fetch_array($salary);
$srSalary = $sal['mek_senior'];$jrSalary = $sal['mek_junior'];
/********** identify meal allowance  ************/
$meal = mysqli_query($conn,"select * from manpower where item = 'Daily Meal'");
$daily = mysqli_fetch_array($meal);
$srMeal = $daily['mek_senior'];$jrMeal = $daily['mek_junior'];
/************************************************/
for ($y = 1; $y < $limit; $y++) {
	$i[$y] = 0;
	$J[$y] = $i[$y];
}
$mp = mysqli_query($conn,"select * from manpower where item != 'Daily Meal'");
while ($row = mysqli_fetch_array($mp)) {
/*
PPH 	(SALARY + THR) X 5%
MEAL ALLOWANCE 	DAILY MEAL X 30 DAYS
MEDICAL ALLOWANCE 	(SALARY X 2) / 12 MONTH
BPJS KESEHATAN + T. KERJA 	SALARY X 11%
THR 	SALARY / 12 MONTH	
*/	
	if($row['item'] == 'THR') {
		$row['mek_senior'] = $srSalary / 12;
		$row['mek_junior'] = $jrSalary / 12;
	}
	if($row['item'] == 'PPH') {
		$row['mek_senior'] = (5/100) * ($srSalary + ($srSalary / 12));
		$row['mek_junior'] = (5/100) * ($jrSalary + ($jrSalary / 12));
	}
	if($row['item'] == 'Meal Allowance') {
		$row['mek_senior'] = $srMeal * 30;
		$row['mek_junior'] = $jrMeal * 30;
	}
	if($row['item'] == 'Medical Allowance') {
		$row['mek_senior'] = ($srSalary * 2) / 12;
		$row['mek_junior'] = ($jrSalary * 2) / 12;
	}
	if($row['item'] == 'BPJS Kesehatan and T. Kerja') {
		$row['mek_senior'] = $srSalary * 11/100;
		$row['mek_junior'] = $jrSalary * 11/100;
	}	
	$all = ($row['mek_senior'] * $new_senior) + ($row['mek_junior'] * $new_junior);$allPay = number_format($all);
	$Y1 = $all * 12; //$Y2 = $Y1 * 1; $Y3 = $Y2 * 1; $Y4 = $Y3 * 1; $Y5 = $Y4 * 1;
	if ($term > 1) {
		$x = 1; $Y[$x] = $Y1; 
		//echo $Y[$x]."<br>";
		for ($z = 1; $z < $limit; $z++) {
			//$Y[$z-1] = $Y1;
			$Y[$z+1] = $Y[$z];
			$Yr[$z] = number_format($Y[$z]);			
			$J[$x] += $Y[$x]; $x++;
		}
	}
	$no++; 
	$srSub = $row['mek_senior'] + $srSub; $jrSub = $row['mek_junior'] + $jrSub; $allSub = $all + $allSub;
}
$Total = 0;$srSubM = $srSub * 1.2; $jrSubM = $jrSub * 1.2; $allSubM = $allSub * 1.2;
for ($v = 1; $v < $limit; $v++) {		
		$X[$v] = $allSubM * 12;
		$Total = $Total + $X[$v];	
}
$TM = number_format($Total);
$CPK = $Total / ($new_mileage * 12 *$term * $new_uio); $CPKM = number_format($CPK);
?>
<!--<div class="row">
<!---------- CONTENT MASTER UNIT --------------->
	<div class="col-lg-6">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-car"></i> Labor Cost / Unit</h3>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-bordered table-hover tablesorter">
						<thead>
							<tr class="bg-primary"><th>Senior Mechanics</th><th>Junior Mechanics</th><th>Total Cost <?php echo $term; ?> Yr</th><th>CPK</th></tr>
						</thead>
						<tbody>
							<tr><td><?php echo $new_senior; ?></td><td><?php echo $new_junior; ?></td><td><?php echo $TM; ?></td><td><?php echo $CPKM; ?></td></tr>
						</tbody>
					</table>
				</div>
				<div class="text-right">
			
				</div>				
			</div>
		</div>
	</div>
<!--</div><!-- /.row -->
<?php 
return $CPK;
}
?>